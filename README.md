# SequenceIO

Template python project for test-driven development

## Contributors

| Name                   | email                                 |
|------------------------|---------------------------------------|
| Carsten Fortmann-Grote | carsten.fortmann-grote@evolbio.mpg.de |
| Natasha Puzovic        | puzovic@evolbio.mpg.de                |
